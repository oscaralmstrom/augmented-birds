﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Transform menu, play, how, small, red, yes, birdBox, birdBall;
    public Text text;
    public AudioSource audioSource;

    private string[] texts = new string[15];

    void Start()
    {
        texts[1] = "Welcome to this birdgame!";
        texts[2] = "Your mission is to dig up as many of these bird-containing balls as you can.";
        texts[3] = "Lucky for you there just happen to exist big birds that will do this for you.";
        texts[4] = "You must then deliver the balls to this floating bird-mailbox.";
        texts[5] = "Lucky for you the big birds will take care of that for you as well.";
        texts[6] = "Mark the spot where you want to dig and watch as the birds do your dirty work.";

        texts[13] = "Sadly the red button was secretly the \"exit button\".";
    }

    // Detta script skrev jag innan jag visste att man kunde
    // använda sig av paneler för att hantera ui på ett bättre sätt
    // så förbered er på tusentals "SetActive()" 

    public void BackToMenu()
    {
        FindObjectOfType<Controller>().game = false;

        menu.gameObject.SetActive(false);

        play.gameObject.SetActive(true);
        how.gameObject.SetActive(true);
        small.gameObject.SetActive(true);
        red.gameObject.SetActive(true);
    }

    public void StartGame() // if the "Start Game" button is pressed
    {
        FindObjectOfType<Controller>().game = true;

        menu.gameObject.SetActive(true);

        play.gameObject.SetActive(false);
        how.gameObject.SetActive(false);
        small.gameObject.SetActive(false);
        red.gameObject.SetActive(false);
    }

    public void HowToPlay() // if the "How to play" button is pressed
    {
        text.gameObject.SetActive(true);
        play.gameObject.SetActive(false);
        how.gameObject.SetActive(false);
        small.gameObject.SetActive(false);
        red.gameObject.SetActive(false);

        yes.gameObject.SetActive(true);
        NextText();
    }

    private bool redButton = false;
    private int textNumber = 0;

    public void NextText() // Decides which text will be printed next
    {
        text.text = "";
        StopAllCoroutines();
        if (!redButton) textNumber++;
        else if (textNumber == 13) Application.Quit();
        else textNumber = 13;
        if (textNumber == 2)
        {
            birdBall.gameObject.SetActive(true);
        }
        else if (textNumber == 4)
        {
            birdBall.gameObject.SetActive(false);
            birdBox.gameObject.SetActive(true);
        }
        else if (textNumber == 7)
        {
            textNumber = 0;
            text.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            how.gameObject.SetActive(true);
            small.gameObject.SetActive(true);
            red.gameObject.SetActive(true);

            birdBox.gameObject.SetActive(false);

            yes.gameObject.SetActive(false);
        }
        if (textNumber != 0 || redButton)
        {
            StartCoroutine(PrintText(texts[textNumber]));
        }
    }

    private float textSpeed = 0.05f;

    private IEnumerator PrintText(string t) // Prints text, one letter at a time
    {
        for (int i = 0; i < t.Length; i++)
        {
            text.text += t[i];
            audioSource.pitch = 2f + Random.Range(-0.5f, 0.5f);
            audioSource.Play();
            yield return new WaitForSeconds(textSpeed);
        }
    }

    public void Small() // if "The Button" is pressed
    {
        RectTransform smallButtonTransform = small.GetComponent<RectTransform>();
        if (smallButtonTransform.localScale.x == 1f)
        {
            smallButtonTransform.localScale = Vector3.one * 0.8f;
            small.GetComponentInChildren<Text>().text = "The small button";
        }
        else if (smallButtonTransform.localScale.x == 0.8f)
        {
            smallButtonTransform.localScale = Vector3.one * 0.6f;
            small.GetComponentInChildren<Text>().text = "The smaller button";
        }
        else if (smallButtonTransform.localScale.x == 0.6f)
        {
            smallButtonTransform.localScale = Vector3.one * 0.4f;
            small.GetComponentInChildren<Text>().text = "The smallest button";
        }
        else if (smallButtonTransform.localScale.x == 0.4f)
        {
            smallButtonTransform.localScale = Vector3.one * 0.2f;
            small.GetComponentInChildren<Text>().text = "";
        }
        else
        {
            smallButtonTransform.localScale = Vector3.one;
            small.GetComponentInChildren<Text>().text = "The Button";
            small.gameObject.SetActive(false);
        }
    }

    public void Red() // if the "Red Button" / "secret exit" button is pressed
    {
        redButton = true;

        text.gameObject.SetActive(true);
        play.gameObject.SetActive(false);
        how.gameObject.SetActive(false);
        small.gameObject.SetActive(false);
        red.gameObject.SetActive(false);

        yes.gameObject.SetActive(true);

        text.color = Color.red;
        NextText();
    }
}
