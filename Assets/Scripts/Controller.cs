﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public ARRaycastManager raycastManager;

    protected Camera c;

    private Touch touch;

    protected RaycastHit hit;

    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    protected float xAxis, yAxis, multiplier = 100f;

    private void Start()
    {
        c = GetComponent<Camera>();
    }

    public bool game = false;

    private void Update()
    {
        if (game)
        {
            Ray ray = c.ScreenPointToRay(Input.mousePosition);

            if (Input.touchCount > 0 &&
                Physics.Raycast(ray, out hit, 5000f))
            {
                InteractWithPlane(hit.transform.GetComponent<HandleBirdOnPlane>());
            }
        }
    }

    protected void InteractWithPlane(HandleBirdOnPlane hbop)
    {
        if (!hbop.aBirdLivesAtThisPlane)
        {
            hbop.SpawnBird(hit.point);
        }
        else
        {
            hbop.MoveBird(hit.point);
        }
    }
}
