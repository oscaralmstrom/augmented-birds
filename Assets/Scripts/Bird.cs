﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour // Script used for both the wheel-bird and the regular one
{
    public bool wheelBird = false;

    public float yPos = 0.215f;
    private float speed = 2f;

    public Transform birdSphere, wheelSphere;
    private Transform ball;

    private Transform birdBody, instantiatedBall, previousBall;
    private Animator animator;

    public delegate void BallDelegate(Transform b);
    public BallDelegate ballDelegate;

    private bool birdIsDoingTheThing = true;

    public delegate void BirdCreatedOrMaybeBirdDestroyed(Bird b);
    public event BirdCreatedOrMaybeBirdDestroyed birdDestroyed;

    struct BirdColors
    {
        public Color bodyColor;
        public Color beakColor;
    }

    private void Start()
    {
        birdColors = RandomColor(birdColors);

        birdBody = transform.GetChild(0);

        Material[] bodyMaterials = birdBody.GetComponent<MeshRenderer>().materials;

        if (wheelBird) // Depending on the type of bird, some things are different
        {
            ball = wheelSphere;
            yPos = 0.12f;
            speed = 1f;

            bodyMaterials[1].color = birdColors.bodyColor;
            bodyMaterials[0].color = birdColors.beakColor;
            birdBody.GetChild(0).GetComponent<MeshRenderer>().materials[2].color = birdColors.beakColor;
        }
        else
        {
            ball = birdSphere;

            bodyMaterials[0].color = birdColors.bodyColor;
            bodyMaterials[1].color = birdColors.beakColor;
            birdBody.GetChild(0).GetComponent<MeshRenderer>().materials[0].color = birdColors.beakColor;
            birdBody.GetChild(1).GetComponent<MeshRenderer>().materials[0].color = birdColors.beakColor;
        }

        FindObjectOfType<BirdHead>().AddBird(this);

        animator = GetComponentInChildren<Animator>();

        transform.localScale = Vector3.zero;
        StartCoroutine(Expand());
    }

    #region Colors

    // Uppenbarligen så behövde jag ungefär 100 rader för att slumpa färgerna

    public Material orange, purple; // Dessa fick jag lägga till för att de inte fanns i klassen "Color"

    BirdColors birdColors = new BirdColors();

    private enum BirdBodyColor { white, yellow, red, orange, blue, green, purple, black }

    private BirdColors RandomColor(BirdColors birdColors) // Randomly generate a color with rarity to apply to the bird
    {
        float r = Random.Range(0f, 100f);

        if (r > 60f) return SetColors(Color.white, BirdBodyColor.white);
        else if (r > 45f) return SetColors(orange.color, BirdBodyColor.orange);
        else if (r > 25f) return SetColors(Color.yellow, BirdBodyColor.yellow);
        else if (r > 15f) return SetColors(Color.red, BirdBodyColor.red);
        else if (r > 5f) return SetColors(Color.blue, BirdBodyColor.blue);
        else if (r > 3f) return SetColors(Color.green, BirdBodyColor.green);
        else if (r > 1f) return SetColors(purple.color, BirdBodyColor.purple);
        else return SetColors(Color.black, BirdBodyColor.black);
    }

    private BirdColors SetColors(Color color, BirdBodyColor birdBodyColor) // Sets the color of the bird
    {
        birdColors.bodyColor = color;
        birdColors.beakColor = BeakColor(birdBodyColor);
        return birdColors;
    }

    private Color BeakColor(BirdBodyColor color) // Unnecessary amounts of work put into the randomization of the color of the beak, legs and wheels
    {
        switch (color)
        {
            case BirdBodyColor.white:
                return BeakColor(Color.red, orange.color, Color.yellow, 50f, 25f, 5f);
            case BirdBodyColor.orange:
                return BeakColor(Color.yellow, Color.red, 25f, 5f);
            case BirdBodyColor.yellow:
                return BeakColor(orange.color, Color.red, 25f, 5f);
            case BirdBodyColor.red:
                return BeakColor(Color.yellow, orange.color, 25f, 5f);
            case BirdBodyColor.blue:
                return BeakColor(orange.color, Color.yellow, Color.red, 50f, 25f, 5f);
            case BirdBodyColor.green:
                return BeakColor(orange.color, Color.yellow, Color.red, 50f, 25f, 5f);
            case BirdBodyColor.purple:
                return BeakColor(orange.color, Color.yellow, Color.red, 50f, 25f, 5f);
            case BirdBodyColor.black:
                return BeakColor(orange.color, Color.yellow, Color.red, 50f, 25f, 5f);
            default:
                return new Color();
        }
    }

    private Color BeakColor(Color color1, Color color2, Color color3, float threshold1, float threshold2, float threshold3)
    {
        float r = Random.Range(0f, 100f);

        if (r > threshold1)
            return color1;
        else if (r > threshold2)
            return color2;
        else if (r > threshold3)
            return color3;
        else
            return RareBeakColor(r);
    }

    private Color BeakColor(Color color1, Color color2, float threshold1, float threshold2)
    {
        float r = Random.Range(0f, 100f);

        if (r > threshold1)
            return color1;
        else if (r > threshold2)
            return color2;
        else
            return RareBeakColor(r);
    }

    private Color RareBeakColor(float r)
    {
        if (r > 2.5f)
            return Color.green;
        else
            return purple.color;
    }
    #endregion 

    private IEnumerator Expand() // Smoother spawn
    {
        while (transform.localScale.x < 0.99f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, 0.2f);
            yield return 0;
        }

        transform.localScale = Vector3.one;

        birdIsDoingTheThing = false;
    }

    public void SubscribeToMovingDelegate(HandleBirdOnPlane hbop) // Gives a reference to this bird
    {
        hbop.delegateMove += MoveTowardsPeckingTarget;
    }

    private void OnDestroy()
    {
        if (birdDestroyed != null) birdDestroyed(this);
    }

    private void MoveTowardsPeckingTarget(Vector3 target)
    {
        if (!birdIsDoingTheThing)
        {
            birdIsDoingTheThing = true;
            StartCoroutine(MoveCoroutine(target));
            animator.SetTrigger("Move");
        }
    }

    private IEnumerator MoveCoroutine(Vector3 target)
    {
        transform.LookAt(target);

        while(Vector3.Distance(transform.position, target) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

            yield return 0;
        }

        animator.SetTrigger("StartPeck");

        StartCoroutine(PeckPeckPeck());
    }

    private IEnumerator PeckPeckPeck()
    {
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        if (instantiatedBall == null)
        {
            StartCoroutine(BallBehaviour());
        }

        birdIsDoingTheThing = false;
    }

    private IEnumerator BallBehaviour() // The behaviour of the created ball
    {
        Vector3 bollSpawnPosition = transform.position
                                    + transform.forward * 0.3f
                                    + transform.up * -0.4f;

        Vector3 bollLerpPosition = new Vector3(bollSpawnPosition.x, transform.position.y, bollSpawnPosition.z);

        instantiatedBall = Instantiate(ball, bollSpawnPosition, Quaternion.identity, null);

        if (wheelBird) // Makes the colors of the small bird inside the ball to be exactly the same as the colors of the bigger bird
        {
            Material[] bollMaterials = new Material[2];
            Transform bollBirdBody = instantiatedBall.transform.GetChild(0);
            bollMaterials = bollBirdBody.GetComponent<MeshRenderer>().materials;
            bollMaterials[1].color = birdColors.bodyColor;
            bollMaterials[0].color = birdColors.beakColor;
            bollBirdBody.GetChild(0).GetComponent<MeshRenderer>().materials[2].color = birdColors.beakColor;
        }
        else // Depending on the type of bird, the meshrenderers might be in other places
        {
            Material[] bollMaterials = new Material[2];
            Transform bollBirdBody = instantiatedBall.transform.GetChild(0);
            bollMaterials = bollBirdBody.GetComponent<MeshRenderer>().materials;
            bollMaterials[0].color = birdColors.bodyColor;
            bollMaterials[1].color = birdColors.beakColor;
            bollBirdBody.GetChild(0).GetComponent<MeshRenderer>().materials[0].color = birdColors.beakColor;
            bollBirdBody.GetChild(1).GetComponent<MeshRenderer>().materials[0].color = birdColors.beakColor;
        }

        if (ballDelegate != null) ballDelegate(instantiatedBall);

        while (instantiatedBall.position.y < transform.position.y - 0.01f) // Makes it look more like the bird is digging the ball up
        {
            instantiatedBall.transform.position = Vector3.Lerp(instantiatedBall.position, bollLerpPosition, Time.deltaTime * 2f);
            yield return 0;
        }
    }
}
