﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdHead : MonoBehaviour
{
    public Transform birdHead;
    private LineRenderer lineRenderer;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void AddBird(Bird birdToAdd) // Activated in the "Bird" class
    {
        birdToAdd.ballDelegate += AddToCollection;
        birdToAdd.birdDestroyed += RemoveBird;
    }

    public void RemoveBird(Bird birdToRemove) // Activated in the "Bird" class
    {
        birdToRemove.ballDelegate -= AddToCollection;
        birdToRemove.birdDestroyed -= RemoveBird;
    }

    private Vector3[] targetPositions = new Vector3[5];

    private bool isMoving = false;

    private List<Transform> birdBallCollection = new List<Transform>();
    private List<Vector3> birdBallSpawnPositions = new List<Vector3>();

    private void AddToCollection(Transform boll) // Adds the created ball to the collection
    {
        birdBallCollection.Add(boll);
        birdBallSpawnPositions.Add(boll.position);
    }

    private void Update()
    {
        if (!isMoving && birdBallCollection.Count > 0) // Moves if the ballcollection is anything but empty
        {
            MoveTowardsPeckingTarget(birdBallSpawnPositions[0] + Vector3.up * 0.5f);
        }
    }

    private void MoveTowardsPeckingTarget(Vector3 raycastHitPosition) // Sets the positions that lead the way to the target ball
    {
        isMoving = true;

        float midPoint = birdHead.position.y - 1f;

        targetPositions[0] = transform.position;
        targetPositions[1] = new Vector3(birdHead.position.x, midPoint, birdHead.position.z);
        targetPositions[2] = new Vector3(raycastHitPosition.x, midPoint, birdHead.position.z);
        targetPositions[3] = new Vector3(raycastHitPosition.x, midPoint, raycastHitPosition.z);
        targetPositions[4] = raycastHitPosition;

        StartCoroutine(MoveTowardsCoroutine());
    }

    private float speed = 2f;

    private IEnumerator MoveTowardsCoroutine() // Moves the birdhead through all targetpositions to reach the target ball
    {
        int currentTarget = 0;
        Vector3[] linePositions = targetPositions;

        while (currentTarget < targetPositions.Length) // Through all targetpositions
        {
            lineRenderer.positionCount = currentTarget + 1;

            while (Vector3.Distance(birdHead.position, targetPositions[currentTarget]) > 0.01f) // Through each specific targetposition 
            {
                birdHead.position = Vector3.MoveTowards(birdHead.position,
                                                        targetPositions[currentTarget],
                                                        Time.deltaTime * speed);
                lineRenderer.SetPositions(SetList(linePositions, currentTarget));
                yield return 0;
            }

            currentTarget++;

            if (currentTarget < 5) birdHead.LookAt(targetPositions[currentTarget]);

            yield return 0;
        }

        birdBallCollection[0].SetParent(birdHead); // Pick up target ball

        currentTarget = 3;

        while (currentTarget != 666) // Through all targetpositions again, but backwards
        {
            while (Vector3.Distance(birdHead.position, targetPositions[currentTarget]) > 0.1f)
            {
                birdHead.position = Vector3.MoveTowards(birdHead.position,
                                        targetPositions[currentTarget],
                                        Time.deltaTime * speed);
                lineRenderer.SetPositions(SetList(linePositions, currentTarget + 1));
                birdHead.LookAt(targetPositions[currentTarget + 1]);
                yield return 0;
            }

            lineRenderer.positionCount = currentTarget + 1;

            if (currentTarget == 0) currentTarget = 667;

            currentTarget--;

            yield return 0;
        }

        Destroy(birdBallCollection[0].gameObject);
        birdBallCollection.RemoveAt(0);
        birdBallSpawnPositions.RemoveAt(0);

        isMoving = false;
    }

    private Vector3[] SetList(Vector3[] linePositions, int currentTarget) // returns the linerendererpositions that represent the neck
    {
        switch (currentTarget)
        {
            default:
                return new Vector3[] { birdHead.position };
            case 1:
                return new Vector3[] { targetPositions[0], birdHead.position };
            case 2:
                return new Vector3[] { targetPositions[0], targetPositions[1], birdHead.position };
            case 3:
                return new Vector3[] { targetPositions[0], targetPositions[1], targetPositions[2], birdHead.position };
            case 4:
                return new Vector3[] { targetPositions[0], targetPositions[1], targetPositions[2], targetPositions[3], birdHead.position };
        }
    }
}
