﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audio : MonoBehaviour
{
    public float audioPitchCenterValue = 1;
    public AudioSource audioSource;
    public AudioClip a, b, c;

    public void Peck() // Plays pecking sounds with variations
    {
        audioSource.pitch = audioPitchCenterValue + Random.Range(-0.5f, 0.5f);

        if (Random.Range(0f, 100f) > 66f)
        {
            audioSource.PlayOneShot(a);
        }
        else if (Random.Range(0f, 100f) > 50f)
        {
            audioSource.PlayOneShot(b);
        }
        else
        {
            audioSource.PlayOneShot(c);
        }
    }
}
