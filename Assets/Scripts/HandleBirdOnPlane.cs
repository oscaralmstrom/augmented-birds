﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleBirdOnPlane : MonoBehaviour
{
    public Transform bird, wheelBird;

    private Transform instantiatedBird;

    public bool aBirdLivesAtThisPlane = false;

    static bool nextBirdHasWheels = false;

    private float birdYPos = 0.215f;

    public void SpawnBird(Vector3 position) // Spawns a bird at "position" 
    {
        aBirdLivesAtThisPlane = true;

        if (nextBirdHasWheels)
        {
            birdYPos = 0.12f;
            instantiatedBird = Instantiate(wheelBird, position + Vector3.up * birdYPos, Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
        }
        else
        {
            instantiatedBird = Instantiate(bird, position + Vector3.up * birdYPos, Quaternion.Euler(0f, Random.Range(0f, 360f), 0f));
        }
        instantiatedBird.GetComponent<Bird>().SubscribeToMovingDelegate(this);

        if (nextBirdHasWheels) nextBirdHasWheels = false;
        else nextBirdHasWheels = true;

    }

    public delegate void InputDelegate(Vector3 raycastHitPosition);
    public event InputDelegate delegateMove;

    public void MoveBird(Vector3 position) // Moves the attached bird to "position" 
    {
        if (delegateMove != null) delegateMove(position + Vector3.up * birdYPos);
    }

    private void OnDestroy() 
    {
        if (instantiatedBird) Destroy(instantiatedBird.GetComponent<Bird>().gameObject);
    }
}
