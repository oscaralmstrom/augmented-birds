﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorTestController : Controller
{
    private void Update()
    {
        xAxis = Input.GetAxis("Mouse X");
        yAxis = Input.GetAxis("Mouse Y");

        transform.Rotate(0f, xAxis * multiplier * Time.deltaTime, 0f);
        c.transform.Rotate(-yAxis * multiplier * Time.deltaTime, 0f, 0f);

        if (game)
        {
            Ray ray = c.ScreenPointToRay(Input.mousePosition);

            if (Input.GetMouseButtonDown(0) &&
                Physics.Raycast(ray, out hit, 5000f))
            {
                InteractWithPlane(hit.transform.GetComponent<HandleBirdOnPlane>());
            }
        }
    }
}
